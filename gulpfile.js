//Loading all the required plugins
var gulp = require('gulp'),
		minifyHTML = require('gulp-minify-html'),
		autoprefixer = require('gulp-autoprefixer'),
		minifyCSS = require('gulp-minify-css'),
		rename = require('gulp-rename'),
		uglify = require('gulp-uglify'),
		jshint = require('gulp-jshint'),
		imagemin = require('gulp-imagemin'),
		plumber = require('gulp-plumber'),
		browserSync = require('browser-sync');

//Variable declarations
var environment = process.env.NODE_ENV || 'development',
		directoryName = __dirname,
		folderName,
		portNumber;

// ******Defining all the tasks*******
// 1. Minifying HTML
gulp.task('min-html', function() {

	//Check all the minifying options available in the comments below
	var options = {
		comments:true,
		conditionals:true
	};
	gulp.src('dev/*.html')
	.pipe(plumber())
	.pipe(minifyHTML(options))
	.pipe(gulp.dest('prod'));
});

//*******Options for minifying provided by min-html plugin********
/*All options are false by default.

empty - do not remove empty attributes
cdata - do not strip CDATA from scripts
comments - do not remove comments
conditionals - do not remove conditional internet explorer comments
spare - do not remove redundant attributes
quotes - do not remove arbitrary quotes

so setting empty:true is the same as telling minifyHTML "do not remove empty attributes."*/

// 2. Minifying CSS
gulp.task('minify-css', function() {
	gulp.src('dev/assets/css/*.css')
	.pipe(autoprefixer({
		remove: false
	}))
	.pipe(gulp.dest('dev/assets/css'))
	.pipe(minifyCSS())
	.pipe(gulp.dest('prod/assets/css'));
});

// 3. Linting and Minifying Javascript
gulp.task('lint-minify-js', function() {
	gulp.src('dev/assets/js/*.js')
	.pipe(plumber())
	.pipe(jshint())
	.pipe(jshint.reporter('jshint-stylish'))
	.pipe(uglify())
	.pipe(gulp.dest('prod/assets/js'));
});

// 4. Compressing images
gulp.task('optimize-image', function(){
	gulp.src('dev/assets/images/*')
	.pipe(imagemin({
		progressive: true,
		svgoPlugins: [{removeViewBox: false}]
	}))
	.pipe(gulp.dest('prod/assets/images'));
});

// 5. Setting up browserSync for Development and production environment.
gulp.task('browser-sync', function() {
	if (environment == 'development') {
		folderName = 'dev';
		portNumber = 3000;
	} else if (environment == 'production') {
		folderName = 'prod';
		portNumber = 3001;
	}

	var projectName = directoryName.split('\\'),
			projName = projectName[projectName.length - 1];

	browserSync({

		browser: ["google chrome", "firefox", "iexplore"],

		files: [folderName+'*.html', folderName+'assets/css/*.css', folderName+'assets/js/*.js'],

		ghostMode: {
			clicks: true,
			forms: true,
			scroll: true
		},

		logFileChanges: true,

		logPrefix: projName,

		port: portNumber,

		server: {
			baseDir: [folderName],
			index: "index.html"
		}
	});
});

// 6. Definining the defualt task for Development branch
gulp.task('default', ['optimize-image','browser-sync'], function() {
	gulp.watch(folderName+'/*.html', ['min-html', browserSync.reload]);
	gulp.watch(folderName+'/assets/js/*js', ['lint-minify-js', browserSync.reload]);
	gulp.watch(folderName+'/assets/css/*.css', ['minify-css', browserSync.reload]);
});
